#include "Operation.hh"

#include <cstdio>
#include <fstream>
#include <memory>
#include <sstream>
#include <utility>
#include "Download.hh"
#include "Local.hh"
#include "Remote.hh"

std::fstream* ReadMirrorFile(MospLocal& local);
void CloseMirrorFile(MospLocal& local, std::fstream* file);

MospOperation::MospOperation(std::vector<std::string> arguments)
    : arguments_(std::move(arguments)) {}

InstallOperation::InstallOperation(std::vector<std::string> arguments)
    : MospOperation(std::move(arguments)) {}

int InstallOperation::Execute() {
  if (arguments_.empty()) {
    std::printf("No package specified\n");
    return 1;
  }

  std::printf("Installing %s\n", arguments_[0].c_str());
  return 0;
}

UninstallOperation::UninstallOperation(std::vector<std::string> arguments)
    : MospOperation(std::move(arguments)) {}

int UninstallOperation::Execute() {
  if (arguments_.empty()) {
    std::printf("No package specified\n");
    return 1;
  }

  std::printf("Uninstalling %s\n", arguments_[0].c_str());
  return 0;
}

UpdateOperation::UpdateOperation(std::vector<std::string> arguments)
    : MospOperation(std::move(arguments)) {}

int UpdateOperation::Execute() {
  if (arguments_.empty()) {
    std::printf("Updating mirrors...\n");

    auto download = MospDownload::GetInstance();
    download.Download(
        "http://gist.githubusercontent.com/eneshalat/"
        "1051b2e35c5e789ba4afa9e78de09a01/raw/"
        "60cd134f3af49cafdbcb837c037470720f1c06ed/remote.json");

    MospLocal local = MospLocal::GetInstance();

    // Mirror path file is in "mirror_file.json"
    std::fstream* remote_file = ReadMirrorFile(local);
    if (remote_file == nullptr) {
      std::printf("Failed to read mirror file\n");
      return 1;
    }

    std::stringstream buffer;
    buffer << remote_file->rdbuf();
    std::string remote_file_content = buffer.str();

    MospRemote remote(remote_file_content);

    CloseMirrorFile(local, remote_file);

    remote.PrintInfo();
    return 0;
  }

  std::printf("Updating %s\n", arguments_[0].c_str());
  return 0;
}

NoOperation::NoOperation(std::vector<std::string> arguments)
    : MospOperation(std::move(arguments)) {}

int NoOperation::Execute() {
  return 0;
}

std::fstream* ReadMirrorFile(MospLocal& local) {
  std::fstream* mirror_file = local.OpenFile("remote.json", std::ios::in);
  return mirror_file;
}

void CloseMirrorFile(MospLocal& local, std::fstream* mirror_file) {
  local.CloseFile(mirror_file);
}