#include "Download.hh"
#include <curl/curl.h>

#include <cstdio>
#include <cstring>
#include <fstream>
#include <ios>
#include <vector>
#include "Base.hh"
#include "Local.hh"

// ptr is the returning response data, size is the size of each element
// nmemb is the number of elements, userdata is the pointer to the data passed to the callback
size_t WriteCallback(char* ptr, size_t size, size_t nmemb, void* userdata);

struct PassBuffer {
  std::vector<char> buffer;
};

MospDownload::MospDownload() {

  curl_global_init(CURL_GLOBAL_DEFAULT);

  curl_ = curl_easy_init();
  if (!curl_) {
    std::printf("CURL INIT FAILED\n");
    curl_global_cleanup();
    return;
  }
}

MospDownload::~MospDownload() {
  curl_easy_cleanup(curl_);
  curl_global_cleanup();
}

MospDownload& MospDownload::GetInstance() {
  static MospDownload instance;
  return instance;
}

void MospDownload::Download(const std::string& url) {
  if (!curl_) {
    std::printf("CURL NOT INITIALIZED\n");
    return;
  }

  int data_len = 0;
  PassBuffer buffer;

  curl_easy_setopt(curl_, CURLOPT_VERBOSE, 0);
  curl_easy_setopt(curl_, CURLOPT_FOLLOWLOCATION, 1);
  curl_easy_setopt(curl_, CURLOPT_URL, url.c_str());
  curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, WriteCallback);
  curl_easy_setopt(curl_, CURLOPT_WRITEDATA, &buffer);

  res_ = curl_easy_perform(curl_);

  if (res_ != CURLE_OK) {
    std::printf("CURL FAILED\n");
    return;
  }

  // Let's dump the returned data to given path.

  MospLocal local = MospLocal::GetInstance();

  std::fstream* file = local.OpenFile(MospLocal::SplitTail(url), std::ios::out);

  std::printf("Saving to \"%s\"...\n", MospLocal::SplitTail(url).c_str());

  file->write(buffer.buffer.data(), buffer.buffer.size());

  local.CloseFile(file);
  // std::printf("--------\n%s\n--------", buffer.buffer.data());
}

// This is the function Curl calls for every chunk of data it downloads
size_t WriteCallback(char* ptr, size_t size, size_t nmemb, void* userdata) {
  // Because the data is not null-terminated, we need to use the
  // function argument to determine its length.

  auto* sized_str = static_cast<PassBuffer*>(userdata);

  sized_str->buffer.reserve(size * nmemb);
  sized_str->buffer.insert(sized_str->buffer.end(), &ptr[0],
                           &ptr[size * nmemb]);

  std::printf("Downloading the file... (%lu : %lu): \n", size * nmemb,
              sized_str->buffer.size());

  return size * nmemb;
}
