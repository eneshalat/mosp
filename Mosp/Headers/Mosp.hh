#ifndef MOSP_MOSP_HH
#define MOSP_MOSP_HH

#include "Base.hh"
#include "Operation.hh"

#define MOSP_NAME "Mosp"
#define MOSP_VERSION "0.0.1"
#define MOSP_AUTHOR "Mosp Team"

class Mosp {
 public:
  Mosp();
  ~Mosp();

  static int Run(int argc, char** argv);

 private:
  static void Usage();
  static void Version();
  static void Help();

  static MospOperation* Command(const std::string& command,
                                const std::vector<std::string>& arguments);
};

#endif