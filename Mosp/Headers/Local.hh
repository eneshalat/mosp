#ifndef MOSP_LOCAL_HH
#define MOSP_LOCAL_HH

#include <fstream>
#include "Base.hh"

class MospLocal {
  std::vector<std::fstream*> open_files_;
  static std::string base_path_;

  MospLocal();

 public:
  ~MospLocal();
  static MospLocal& GetInstance();

  static std::string ExtendPath(const std::string& path);
  static std::string GetBasePath();
  static std::string ConvertHomePath(const std::string& path);
  std::fstream* OpenFile(const std::string& path, std::ios_base::openmode mode);
  void CloseFile(std::fstream* file);
  static std::string SplitTail(const std::string& path);
};

#endif