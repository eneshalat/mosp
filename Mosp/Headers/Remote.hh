#ifndef MOSP_REMOTE_HH
#define MOSP_REMOTE_HH

#include <json/json.h>
#include <memory>
#include "Base.hh"

enum class MospDefinitionType { kPackage, kSource, kUnknown };

class MospDefinition {

 protected:
  std::string name_;
  std::string version_;
  std::string author_;

  std::string license_;
  std::string description_;

  MospDefinitionType type_ = MospDefinitionType::kUnknown;

 public:
  MospDefinition();
  virtual ~MospDefinition() = default;

  std::string Name();
  std::string Version();
  std::string Author();
  std::string License();
  std::string Description();
  MospDefinitionType Type();

  virtual int ParseFromJson(const Json::Value& json) = 0;
};

class MospPackageDefinition : public MospDefinition {

  std::string package_;
  std::string checksum_;

  MospDefinitionType type_ = MospDefinitionType::kPackage;

 public:
  MospPackageDefinition();
  explicit MospPackageDefinition(const std::string& json);

  std::string Package();
  std::string Checksum();

  int ParseFromJson(const Json::Value& json) override;
};

class MospSourceDefinition : public MospDefinition {

  std::string source_;
  std::string commit_;

  MospDefinitionType type_ = MospDefinitionType::kSource;

 public:
  MospSourceDefinition();
  explicit MospSourceDefinition(const std::string& json);

  std::string Source();
  std::string Commit();

  int ParseFromJson(const Json::Value& json) override;
};

struct MospRemoteInfo {
  std::string name;
  std::string description;
  std::string url;
  std::string status;

  int package_count;
};

class MospRemote {

 public:
  MospRemoteInfo info_;
  std::vector<std::shared_ptr<MospDefinition>> packages_;

  MospRemote();
  explicit MospRemote(const std::string& json);
  ~MospRemote();

  int ParseFromJson(const std::string& json);
  void PrintInfo();
};

#endif