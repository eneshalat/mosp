#ifndef MOSP_OPERATION_HH
#define MOSP_OPERATION_HH

#include "Base.hh"

class MospOperation {

 protected:
  std::vector<std::string> arguments_;

 public:
  explicit MospOperation(std::vector<std::string> arguments);
  virtual int Execute() = 0;
  virtual ~MospOperation() = default;
};

class InstallOperation : public MospOperation {
 public:
  explicit InstallOperation(std::vector<std::string> arguments);
  int Execute() override;
};

class UninstallOperation : public MospOperation {
 public:
  explicit UninstallOperation(std::vector<std::string> arguments);
  int Execute() override;
};

class UpdateOperation : public MospOperation {
 public:
  explicit UpdateOperation(std::vector<std::string> arguments);
  int Execute() override;
};

class NoOperation : public MospOperation {
 public:
  explicit NoOperation(std::vector<std::string> arguments);
  int Execute() override;
};

#endif