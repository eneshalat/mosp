#ifndef MOSP_PACKAGE_HH
#define MOSP_PACKAGE_HH

#include "Base.hh"

class MospPackage {
  std::string name_;
  std::string version_;

  std::string path_;

 public:
  MospPackage();
  MospPackage(const std::string& name, const std::string& version);
  ~MospPackage();

  int Install();
  int Uninstall();
};

#endif