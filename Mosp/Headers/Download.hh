#ifndef MOSP_DOWNLOAD_HH
#define MOSP_DOWNLOAD_HH

#include <curl/curl.h>
#include "Base.hh"

class MospDownload {

  CURL* curl_;
  CURLcode res_;

  MospDownload();

 public:
  ~MospDownload();
  static MospDownload& GetInstance();
  void Download(const std::string& url);
};

#endif