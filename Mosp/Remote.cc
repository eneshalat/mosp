#include "Remote.hh"
#include <json/json.h>
#include <memory>

MospDefinition::MospDefinition() = default;

std::string MospDefinition::Name() {
  return name_;
}

std::string MospDefinition::Version() {
  return version_;
}

std::string MospDefinition::Author() {
  return author_;
}

std::string MospDefinition::License() {
  return license_;
}

std::string MospDefinition::Description() {
  return description_;
}

MospDefinitionType MospDefinition::Type() {
  return type_;
}

MospPackageDefinition::MospPackageDefinition() = default;

MospPackageDefinition::MospPackageDefinition(const std::string& json) {
  ParseFromJson(json);
}

std::string MospPackageDefinition::Package() {
  return package_;
}

std::string MospPackageDefinition::Checksum() {
  return checksum_;
}

int MospPackageDefinition::ParseFromJson(const Json::Value& json) {

  // Extract values from the JSON object
  name_ = json["name"].asString();
  version_ = json["version"].asString();
  author_ = json["author"].asString();
  license_ = json["license"].asString();
  description_ = json["description"].asString();
  package_ = json["url"].asString();
  checksum_ = json["checksum_file"].asString();

  // Optionally, validate or perform additional checks on the extracted values

  return 0;  // Parsing successful
}

MospSourceDefinition::MospSourceDefinition() = default;

MospSourceDefinition::MospSourceDefinition(const std::string& json) {
  ParseFromJson(json);
}

std::string MospSourceDefinition::Source() {
  return source_;
}

std::string MospSourceDefinition::Commit() {
  return commit_;
}

int MospSourceDefinition::ParseFromJson(const Json::Value& json) {

  // Extract values from the JSON object
  name_ = json["name"].asString();
  version_ = json["version"].asString();
  author_ = json["author"].asString();
  license_ = json["license"].asString();
  description_ = json["description"].asString();
  source_ = json["source"].asString();
  commit_ = json["commit"].asString();

  // Optionally, validate or perform additional checks on the extracted values

  return 0;
}

MospRemote::MospRemote() {
  info_.name = "";
  info_.description = "";
  info_.url = "";
  info_.status = "";
  info_.package_count = 0;
}

MospRemote::MospRemote(const std::string& json) {
  ParseFromJson(json);
}

MospRemote::~MospRemote() = default;

int MospRemote::ParseFromJson(const std::string& json) {
  Json::Value root;
  Json::Reader reader;

  // Parse the JSON string
  if (!reader.parse(json, root)) {
    // JSON parsing failed
    return -1;
  }

  Json::Value remote = root["remote_info"];
  Json::Value packages = root["packages"];

  // Extract values from the JSON object
  info_.name = remote["name"].asString();
  info_.description = remote["description"].asString();
  info_.url = remote["url"].asString();
  info_.status = remote["status"].asString();
  info_.package_count = remote["package_count"].asInt();

  // Extract packages from the JSON object
  for (int i = 0; i < info_.package_count; i++) {
    MospPackageDefinition package;
    package.ParseFromJson(packages[i]);

    auto package_ptr = std::make_shared<MospPackageDefinition>(package);
    packages_.push_back(package_ptr);
  }

  return 0;
}

void MospRemote::PrintInfo() {
  std::printf("Remote info: \n");
  std::printf("Name: %s\n", this->info_.name.c_str());
  std::printf("Desc: %s\n", this->info_.description.c_str());
  std::printf("URL: %s\n", this->info_.url.c_str());

  std::printf("Packages: \n");
  for (std::shared_ptr<MospDefinition>& def : this->packages_) {
    std::shared_ptr<MospPackageDefinition> package =
        std::dynamic_pointer_cast<MospPackageDefinition>(def);

    std::printf("Name: %s\n", package->Name().c_str());
    std::printf("Version: %s\n", package->Version().c_str());
    std::printf("Author: %s\n", package->Author().c_str());
    std::printf("License: %s\n", package->License().c_str());
    std::printf("Desc: %s\n", package->Description().c_str());
    std::printf("Package: %s\n", package->Package().c_str());
    std::printf("Checksum: %s\n", package->Checksum().c_str());
  }
}