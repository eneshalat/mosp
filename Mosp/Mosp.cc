#include "Mosp.hh"

#include <iostream>
#include "Operation.hh"

Mosp::Mosp() = default;

// Mosp::~Mosp() = default;

int Mosp::Run(int argc, char** argv) {

  if (argc > 1) {

    std::string command = argv[1];
    std::vector<std::string> arguments;
    for (int i = 2; i < argc; i++) {
      arguments.emplace_back(argv[i]);
    }

    MospOperation* operation = Mosp::Command(command, arguments);
    auto res = operation->Execute();
    delete operation;
    return res;
  }

  Mosp::Usage();
  return 0;
}

MospOperation* Mosp::Command(const std::string& command,
                             const std::vector<std::string>& arguments) {
  if (command == "help") {
    Mosp::Help();
  } else if (command == "version") {
    Mosp::Version();
  }

  if (command == "install") {
    return new InstallOperation(arguments);
  }
  if (command == "uninstall") {
    return new UninstallOperation(arguments);
  }
  if (command == "update") {
    return new UpdateOperation(arguments);
  }

  return new NoOperation(arguments);
}

void Mosp::Usage() {
  std::cout << "Usage: mosp [command] [arguments]" << std::endl;
  std::cout << "Try 'mosp help' for more information." << std::endl;
}

void Mosp::Version() {
  std::cout << MOSP_NAME << " " << MOSP_VERSION << std::endl;
}

void Mosp::Help() {
  std::cout << "Usage: mosp [command] [arguments]" << std::endl;
  std::cout << std::endl;
  std::cout << "Commands:" << std::endl;
  std::cout << "  help    Show this help message and exit." << std::endl;
  std::cout << "  version Show version information and exit." << std::endl;
}