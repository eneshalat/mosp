#include "Local.hh"

#include <json/json.h>
#include <algorithm>
#include <cstdio>
#include <fstream>

std::string MospLocal::base_path_ = GetBasePath();

MospLocal::MospLocal() = default;

MospLocal::~MospLocal() {
  for (auto* file : open_files_) {
    file->close();
    delete file;
  }
}

MospLocal& MospLocal::GetInstance() {
  static MospLocal instance;
  return instance;
}

std::string MospLocal::ExtendPath(const std::string& path) {
  return base_path_ + "/" + path;
}

std::string MospLocal::GetBasePath() {
  return ConvertHomePath("~/.mosp");
}

std::string MospLocal::ConvertHomePath(const std::string& path) {
  if (path[0] == '~') {
    return getenv("HOME") + path.substr(1);
  }
  return path;
}

std::fstream* MospLocal::OpenFile(const std::string& path,
                                  std::ios_base::openmode mode) {
  auto* file = new std::fstream(ExtendPath(path), mode);
  // std::printf("Opening file %s\n", ExtendPath(path).c_str());

  if (file == nullptr || !file->is_open()) {
    std::printf("Failed to open file %s\n", ExtendPath(path).c_str());
    return nullptr;
  }

  open_files_.push_back(file);
  return file;
}

void MospLocal::CloseFile(std::fstream* file) {
  file->close();
  open_files_.erase(std::find(open_files_.begin(), open_files_.end(), file));
  delete file;
}

std::string MospLocal::SplitTail(const std::string& path) {
  auto pos = path.find_last_of('/');
  if (pos == std::string::npos) {
    return "";
  }
  return path.substr(pos + 1);
}